// import express
const express = require("express");

// import middleware
const {orderMiddleware} = require("../middlewares/orderMiddleware");

// import controllers
const {
    getAllOrdersOfAnUser,
    getOrderByOrderID,
    createOrder,
    updateOrder,
    deleteOrder
} = require("../controllers/orderControllers");

// Khoi tao router
const orderRouter = express.Router();

// Su dung Middleware
orderRouter.use(orderMiddleware);

// get all order
orderRouter.get("/orders", getAllOrdersOfAnUser);

// get order by ID
orderRouter.get("/orders/:orderID", getOrderByOrderID);

// Create order
orderRouter.post("/orders", createOrder);

// Update order 
orderRouter.put("/orders/:orderID", updateOrder);

// delete order 
orderRouter.delete("/orders/:orderID", deleteOrder);

module.exports =  {orderRouter};