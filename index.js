// Import thu vien express
const express = require("express");

// import thu vien path
const path = require("path");

// Import thu vien mongoose
const mongoose = require("mongoose");

// Import router
const { drinkRouter } = require("./app/routes/drinkRouter");
const { orderRouter } = require("./app/routes/orderRouter");
const { userRouter } = require("./app/routes/userRouter");
const { voucherRouter } = require("./app/routes/voucherRouter");


mongoose.connect('mongodb://localhost:27017/CRUD-pizza-project', (error) => {
    if (error) throw error;
    console.log("Successfully connect!!!");
}) 

// create app
const app = express();

// Tao port 
const port = 8000;


//Khai bao API de chay trang pizza 365
app.get("/", (req, res) => {
    console.log(__dirname);

    res.sendFile(path.join(__dirname + "/views/pizza365.html"));
})

// them middleware de hien thi anh
app.use(express.static(__dirname + "/views"));

// Khai bao API de chay orderlist

app.get("/orderList", (req,res) => {
    console.log(__dirname);

    res.sendFile(path.join(__dirname + "/views/order.html"));
})


// Su dung body json
app.use(express.json());

// Su dung dc unicode 
app.use(express.urlencoded)({
    extended: true
});

// user router
app.use(drinkRouter);
app.use(orderRouter);
app.use(voucherRouter);
app.use(userRouter);

app.listen(port, () =>{
    console.log(`App listening on port ${port}`)
})
