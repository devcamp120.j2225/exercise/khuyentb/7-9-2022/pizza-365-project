// import express
const express = require("express");

// import middleware
const {voucherMiddleware} = require("../middlewares/voucherMiddlewares");

// import controllers
const {
    getAllVouchers,
    getVoucherByID,
    createVoucher,
    updateVoucher,
    deleteVoucher
} = require("../controllers/voucherControllers");

// Khoi tao router
const voucherRouter = express.Router();

// Su dung Middleware
voucherRouter.use(voucherMiddleware);

// get all order
voucherRouter.get("/vouchers", getAllVouchers);

// get order by ID
voucherRouter.get("/vouchers/:voucherID", getVoucherByID);

// Create order
voucherRouter.post("/vouchers", createVoucher);

// Update order 
voucherRouter.put("/vouchers/:voucherID", updateVoucher);

// delete order 
voucherRouter.delete("/vouchers/:voucherID", deleteVoucher);

module.exports =  {voucherRouter};