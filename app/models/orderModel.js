// Import thu vien mongoose
const mongoose = require("mongoose");

// Khai bao Schema
const Schema = mongoose.Schema;

// Khai bao order schema
const orderModel = new Schema({
    _id: {
        type: mongoose.Types.ObjectId,
        required: true 
    },
    orderCode: {
        type: String,
        required: true,
        unique: true 
    }, 
    pizzaSize: {
        type: String,
        required: true 
    },
    pizzaType: {
        type: String,
        required: true
   },
   voucher: {
        type: Schema.Types.ObjectId,
        ref: 'Voucher'
   },
   drink: {
        type: Schema.Types.ObjectId,
        ref: 'Drink'
   },
   status: {
        type: String,
        required: true
   }
}, {
   timestamps: true
})

module.exports = mongoose.model('Order', orderModel);