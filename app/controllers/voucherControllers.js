// Import thư viện mongoose
const mongoose = require('mongoose');

// Import voucher model
const voucherModel = require('../models/voucherModel');

// Get all vouchers
const getAllVouchers = (req, res) => {
     // B1: Thu thập dữ liệu từ req
     // B2: Validate dữ liệu
     // B3: Gọi model thực hiện các thao tác nghiệp vụ
     voucherModel.find((error, data) => {
          if (error) {
               return res.status(500).json({
                    message: error.message
               })
          }
          return res.status(200).json({
               message: 'Get all vouchers successfully',
               allVouchers: data
          })
     })
}

// Get voucher by ID
const getVoucherByID = (req, res) => {
     // B1: Thu thập dữ liệu từ req
     let voucherID = req.params.voucherID;
     // B2: Validate dữ liệu
     if (!mongoose.Types.ObjectId.isValid(voucherID)) {
          return res.status(400).json({
               message: 'Voucher ID is invalid!'
          })
     }
     // B3: Gọi model thực hiện các thao tác nghiệp vụ
     voucherModel.findById(voucherID, (error, data) => {
          if (error) {
               return res.status(500).json({
                    message: error.message
               })
          }
          return res.status(200).json({
               message: 'Get a voucher by ID',
               voucher: data
          })
     })
}

// Create a voucher
const createVoucher = (req, res) => {
     // B1: Thu thập dữ liệu từ req
     let body = req.body;
     // B2: Validate dữ liệu
     if (!body.maVoucher) {
          return res.status(400).json({
               message: 'Voucher Code is required!'
          })
     }
     if (!Number.isInteger(body.phanTramGiamGia) || body.phanTramGiamGia < 0) {
          return res.status(400).json({
               message: 'Discount Percent is invalid!'
          })
     }
     // B3: Gọi model thực hiện các thao tác nghiệp vụ
     let newVoucher = {
          _id: mongoose.Types.ObjectId(),
          maVoucher: body.maVoucher,
          phanTramGiamGia: body.phanTramGiamGia,
          ghiChu: body.ghiChu
     }
     voucherModel.create(newVoucher, (error, data) => {
          if (error) {
               return res.status(500).json({
                    message: error.message
               })
          }
          return res.status(201).json({
               message: 'Create successfully',
               voucher: data
          })
     })
}

// Update a voucher
const updateVoucher = (req, res) => {
     // B1: Thu thập dữ liệu từ req
     let voucherID = req.params.voucherID;
     let body = req.body;
     // B2: Validate dữ liệu
     if (!mongoose.Types.ObjectId.isValid(voucherID)) {
          return res.status(400).json({
               message: 'Voucher ID is invalid!'
          })
     }
     if (body.maVoucher !== undefined && body.maVoucher == '') {
          return res.status(400).json({
               message: 'Voucher Code is required!'
          })
     }
     if (body.phanTramGiamGia !== undefined && !Number.isInteger(body.phanTramGiamGia) || body.phanTramGiamGia < 0) {
          return res.status(400).json({
               message: 'Discount Percent is invalid!'
          })
     }
     // B3: Gọi model thực hiện các thao tác nghiệp vụ
     let voucherUpdate = {
          maVoucher: body.maVoucher,
          phanTramGiamGia: body.phanTramGiamGia,
          ghiChu: body.ghiChu
     }
     voucherModel.findByIdAndUpdate(voucherID, voucherUpdate, (error, data) => {
          if (error) {
               return res.status(500).json({
                    message: error.message
               })
          }
          return res.status(200).json({
               message: 'Update successfully',
               voucher: data
          })
     })
}

// Delete a voucher
const deleteVoucher = (req, res) => {
     // B1: Thu thập dữ liệu từ req
     let voucherID = req.params.voucherID;
     // B2: Validate dữ liệu
     if (!mongoose.Types.ObjectId.isValid(voucherID)) {
          return res.status(400).json({
               message: 'Voucher ID is invalid!'
          })
     }
     // B3: Gọi model thực hiện các thao tác nghiệp vụ
     voucherModel.findByIdAndDelete(voucherID, (error, data) => {
          if (error) {
               return res.status(500).json({
                    message: error.message
               })
          }
          return res.status(204).json({
               message: 'Delete successfully'
          })
     })
}

module.exports = { getAllVouchers, getVoucherByID, createVoucher, updateVoucher, deleteVoucher };
