// Import mongoose
const mongoose = require("mongoose");

// Khai bao Schema
const Schema = mongoose.Schema;

// Khai bao srink schema 
const drinkModel = new Schema ({
    _id: {
        type: Schema.Types.ObjectId,
        unique: true
    },
    maNuocUong: {
        type: String,
        unique: true,
        required: true
    },
    tenNuocUong: {
        type: String,
        required: true
    },
    donGia : {
        type: Number,
        required: true
    },
    ngayTao: {
        type: Date,
        default: Date.now()
    },
    ngayCapNhat: {
        type: Date,
        default: Date.now()
    }
}, {
    timestamps: true
})

module.exports = mongoose.model("drinks", drinkModel);