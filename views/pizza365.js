"use strict";
$(document).ready(function () {

    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    const gBASE_URL = "http://42.115.221.44:8080/devcamp-pizza365/orders";
    const gDRINKS_URL = "http://42.115.221.44:8080/devcamp-pizza365/drinks";
    const gVOUCHER_URL = "http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/";

    var gSelectedMenu = {
        menuName: "",
        duongKinh: "",
        suonNuong: "",
        salad: "",
        nuocNgot: "",
        thanhTien: ""
    };

    var gPizzaType = "";

    var gPercentDiscount = 0;

    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    // gán sự kiện nút chọn combo S
    $("#btn-small").click(onBtnSmallClick);
    // gán sự kiện nút chọn combo S
    $("#btn-medium").click(onBtnMediumClick);
    // gán sự kiện nút chọn combo S
    $("#btn-large").click(onBtnLargeClick);
    // gán sự kiện nút chọn pizza hải sản
    $("#btn-seafood").click(onBtnSeafoodClick);
    // gán sự kiện nút chọn pizza hawaii
    $("#btn-hawaii").click(onBtnHawaiiClick);
    // gán sự kiện nút chọn pizza bacon
    $("#btn-bacon").click(onBtnBaconClick);
    // gán sự kiện tải trang
    onPageLoading();
    // gán sự kiện cho nút Gửi đơn
    $("#btn-send-info").click(onBtnSendOrderClick);
    // gán sự kiện cho nút Tạo đơn
    $("#btn-create-order").click(onBtnCreateOrderClick);

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    // Hàm xử lý sự kiện nút chọn combo S
    function onBtnSmallClick() {
        // tạo đối tượng đc tham số hoá
        var vSelectedMenu = getSelectedMenu("Small", 20, 2, 200, 2, 150000);
        // method hiển thị ra console
        vSelectedMenu.displayInConsoleLog();
        // gán vào biến toàn cục
        gSelectedMenu = vSelectedMenu;
    }

    // Hàm xử lý sự kiện nút chọn combo M
    function onBtnMediumClick() {
        // tạo đối tượng
        var vSelectedMenu = getSelectedMenu("Medium", 25, 4, 300, 3, 200000);
        // method hiển thị ra console
        vSelectedMenu.displayInConsoleLog();
        // gán vào biến toàn cục
        gSelectedMenu = vSelectedMenu;
    }

    // Hàm xử lý sự kiện nút chọn combo L
    function onBtnLargeClick() {
        // tạo đối tượng
        var vSelectedMenu = getSelectedMenu("Large", 30, 8, 500, 4, 250000);
        // method hiển thị ra console
        vSelectedMenu.displayInConsoleLog();
        // gán vào biến toàn cục
        gSelectedMenu = vSelectedMenu;
    }

    // Hàm xử lý sự kiện nút chọn pizza hải sản
    function onBtnSeafoodClick() {
        // tạo đối tượng được tham số hoá
        var vPizzaType = getPizzaType("Pizza Hải Sản");
        // method hiển thị ra console
        vPizzaType.displayInConsole();
        // gán vào biến toàn cục
        gPizzaType = vPizzaType;
    }

    // Hàm xử lý sự kiện nút chọn pizza hawaii
    function onBtnHawaiiClick() {
        // tạo đối tượng
        var vPizzaType = getPizzaType("Pizza Hawaii");
        // method hiển thị ra console
        vPizzaType.displayInConsole();
        // gán vào biến toàn cục
        gPizzaType = vPizzaType;
    }

    // Hàm xử lý sự kiện nút chọn pizza bacon
    function onBtnBaconClick() {
        // tạo đối tượng
        var vPizzaType = getPizzaType("Pizza Thịt Heo Xông Khói");
        // method hiển thị ra console
        vPizzaType.displayInConsole();
        // gán vào biến toàn cục
        gPizzaType = vPizzaType;
    }

    // Hàm xử lý sự kiện tải trang
    function onPageLoading() {
        callAPIDrinks();
    }

    // Hàm xử lý sự kiện nút Gửi đơn
    function onBtnSendOrderClick() {
        var vOrder = {
            menuName: "",
            pizzaType: "",
            drink: "",
            drinkName: "",
            fullName: "",
            email: "",
            phoneNumber: "",
            address: "",
            voucherID: "",
            message: "",
            price: "",
            percentDiscount: 0,
            priceActual: function () {
                return (this.price * (1 - gPercentDiscount / 100));
            }
        }
        // B1: Thu thập
        getOrderData(vOrder);
        // B2: Kiểm tra
        var vCheckOrderData = validateOrderData(vOrder);
        if (vCheckOrderData) {
            if (vOrder.voucherID != "") {
                callAPIVoucher(vOrder.voucherID);
                // B3: Hiển thị
                console.log(vOrder);
                $("#order-info-modal").modal("show");
                showOrderDetailsInModal(vOrder);
            } else {
                gPercentDiscount = 0;
                $("#order-info-modal").modal("show");
                showOrderDetailsInModal(vOrder);
            }
        }
    }

    // Hàm xử lý sự kiện nút Tạo đơn
    function onBtnCreateOrderClick() {
        $("#order-info-modal").modal("hide");
        $("#modal-create-order").modal("show");
        var vCreateOrder = {
            kichCo: "",
            duongKinh: "",
            suon: "",
            salad: "",
            loaiPizza: "",
            idVourcher: "",
            idLoaiNuocUong: "",
            soLuongNuoc: "",
            hoTen: "",
            thanhTien: "",
            email: "",
            soDienThoai: "",
            diaChi: "",
            loiNhan: ""
        }
        // B1: Thu thập
        getNewOrderData(vCreateOrder);
        // B2: Kiểm tra (ko cần)
        // B3: Gọi API tạo đơn
        sendPostRequestToAPI(vCreateOrder);
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình */
    // Hàm trả lại đối tượng đc tham số hoá
    function getSelectedMenu(paramMenuName, paramDuongKinh, paramSuon, paramSalad, paramNuocNgot, paramThanhTien) {
        var vMenu = {
            menuName: paramMenuName,
            duongKinh: paramDuongKinh,
            suonNuong: paramSuon,
            salad: paramSalad,
            nuocNgot: paramNuocNgot,
            thanhTien: paramThanhTien,
            // hiển thị ra console
            displayInConsoleLog() {
                console.log("%cMenu được chọn là: " + this.menuName, "color: red");
                console.log("Đường kính: " + this.duongKinh + "cm");
                console.log("Sườn nướng: " + this.suonNuong);
                console.log("Salad: " + this.salad + "gr");
                console.log("Nước ngọt: " + this.nuocNgot);
                console.log("Thành tiền: " + this.thanhTien + " VNĐ");
            }
        }
        return vMenu;
    }

    // Hàm trả lại đối tượng pizza type đc tham số hoá
    function getPizzaType(paramPizzaType) {
        var vPizzaType = {
            pizzaType: paramPizzaType,
            // hiển thị ra console
            displayInConsole() {
                console.log("%cPizza được chọn là: " + this.pizzaType, "color: blue");
            }
        }
        return vPizzaType;
    }

    // Hàm gọi API Drinks
    function callAPIDrinks() {
        $.ajax({
            url: gDRINKS_URL,
            type: "GET",
            success: function (paramDrinks) {
                // console.log(paramDrinks);
                loadDrinksToSelect(paramDrinks);
            },
            error: function (paramError) {
                console.log(paramError.responseText);
            }
        })
    }

    // Hàm đổ dữ liệu drinks vào ô select
    function loadDrinksToSelect(paramDrinks) {
        var vSelectDrinks = $("#select-drink");
        for (var i = 0; i < paramDrinks.length; i++) {
            $("<option>", {
                value: paramDrinks[i].maNuocUong,
                text: paramDrinks[i].tenNuocUong
            }).appendTo(vSelectDrinks);
        }
    }

    // Hàm thu thập dữ liệu nút Gửi đơn
    function getOrderData(paramOrderData) {
        paramOrderData.menuName = gSelectedMenu.menuName;
        paramOrderData.pizzaType = gPizzaType.pizzaType;
        paramOrderData.drink = $("#select-drink").val();
        paramOrderData.drinkName = $("#select-drink option:selected").text();
        paramOrderData.fullName = $("#inp-name").val().trim();
        paramOrderData.email = $("#inp-email").val().trim();
        paramOrderData.phoneNumber = $("#inp-phone-number").val().trim();
        paramOrderData.address = $("#inp-address").val().trim();
        paramOrderData.voucherID = $("#inp-voucher").val().trim();
        paramOrderData.message = $("#inp-message").val().trim();
        paramOrderData.price = gSelectedMenu.thanhTien;
        paramOrderData.percentDiscount = gPercentDiscount;
    }

    // Hàm kiểm tra dữ liệu nút Gửi đơn
    function validateOrderData(paramOrderData) {
        if (paramOrderData.menuName == "") {
            alert("Bạn chưa chọn Menu!");
            return false;
        }
        if (paramOrderData.pizzaType == null) {
            alert("Bạn chưa chọn loại Pizza!");
            return false;
        }
        if (paramOrderData.drink == "null") {
            alert("Bạn chưa chọn đồ uống!");
            return false;
        }
        if (paramOrderData.fullName == "") {
            alert("Bạn chưa điền Họ và Tên!");
            $("#inp-name").focus();
            return false;
        }
        var vRegexStr = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        if (!vRegexStr.test(paramOrderData.email)) {
            alert("Email không hợp lệ!");
            $("#inp-email").focus();
            return false;
        }
        if (paramOrderData.phoneNumber == "") {
            alert("Bạn chưa điền SĐT!");
            $("#inp-phone-number").focus();
            return false;
        }
        if (paramOrderData.address == "") {
            alert("Bạn chưa điền Địa chỉ!");
            $("#inp-address").focus();
            return false;
        }
        return true;
    }

    // Hàm gọi API voucher
    function callAPIVoucher(paramVoucherID) {
        $.ajax({
            url: gVOUCHER_URL + paramVoucherID,
            type: "GET",
            async: false,
            success: function (paramVoucher) {
                // console.log(paramVoucher);
                gPercentDiscount = paramVoucher.phanTramGiamGia;
            },
            error: function (error) {
                gPercentDiscount = 0;
            }
        })
    }

    // Hàm hiển thị thông tin đơn hàng lên modal
    function showOrderDetailsInModal(paramOrderData) {
        $("#inp-modal-name").val(paramOrderData.fullName);
        $("#inp-modal-email").val(paramOrderData.email);
        $("#inp-modal-phone-number").val(paramOrderData.phoneNumber);
        $("#inp-modal-address").val(paramOrderData.address);
        $("#inp-modal-voucher").val(paramOrderData.voucherID);
        $("#inp-modal-message").val(paramOrderData.message);
        $("#inp-modal-combo").val(paramOrderData.menuName);
        $("#inp-modal-pizza").val(paramOrderData.pizzaType);
        $("#inp-modal-drink").val(paramOrderData.drinkName);
        $("#inp-modal-price").val(paramOrderData.price + " VNĐ");
        $("#inp-modal-discount").val(gPercentDiscount + "%");
        $("#inp-modal-price-actual").val(paramOrderData.priceActual() + " VNĐ");
        $("#inp-modal-other-details").val("Sườn Nướng: " + gSelectedMenu.suonNuong +
            ", Salad: " + gSelectedMenu.salad + "gr" + ", Số lượng nước: " + gSelectedMenu.nuocNgot);
    }

    // Hàm thu thập dữ liệu nút Tạo đơn
    function getNewOrderData(paramNewOrderData) {
        paramNewOrderData.kichCo = gSelectedMenu.menuName;
        paramNewOrderData.duongKinh = gSelectedMenu.duongKinh;
        paramNewOrderData.suon = gSelectedMenu.suonNuong;
        paramNewOrderData.salad = gSelectedMenu.salad;
        paramNewOrderData.loaiPizza = gPizzaType.pizzaType;
        paramNewOrderData.idVourcher = $.trim($("#inp-modal-voucher").val());
        paramNewOrderData.idLoaiNuocUong = $("#select-drink").val();
        paramNewOrderData.soLuongNuoc = gSelectedMenu.nuocNgot;
        paramNewOrderData.hoTen = $.trim($("#inp-modal-name").val());
        paramNewOrderData.thanhTien = gSelectedMenu.thanhTien;
        paramNewOrderData.email = $.trim($("#inp-modal-email").val());
        paramNewOrderData.soDienThoai = $.trim($("#inp-modal-phone-number").val());
        paramNewOrderData.diaChi = $.trim($("#inp-modal-address").val());
        paramNewOrderData.loiNhan = $.trim($("#inp-modal-message").val());
    }


    // Hàm gọi API tạo đơn
    function sendPostRequestToAPI(paramNewOrderData) {
        $.ajax({
            url: gBASE_URL,
            type: "POST",
            contentType: "application/json;charset=UTF-8",
            data: JSON.stringify(paramNewOrderData),
            success: function (paramCreateSuccess) {
                console.log(paramCreateSuccess);
                // B4: Hiển thị
                showResultToModal(paramCreateSuccess);
            },
            error: function (error) {
                console.log(error.responseText);
            }
        })
    }

    // Hàm hiển thị orderId lên modal
    function showResultToModal(paramNewOrder) {
        $("#inp-order-code").val(paramNewOrder.orderId);
    }

})
