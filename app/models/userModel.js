// Import thư viện mongoose
const mongoose = require('mongoose');

// Khai báo class schema
const Schema = mongoose.Schema;

// Khai báo user schema
const userModel = new Schema({
     _id: {
          type: mongoose.Types.ObjectId
     },
     fullName: {
          type: String,
          required: true
     },
     email: {
          type: String,
          unique: true,
          required: true
     },
     address: {
          type: String,
          required: true
     },
     phone: {
          type: String,
          unique: true,
          required: true
     },
     orders: [{
          type: mongoose.Types.ObjectId,
          ref: 'Order'
     }]
}, {
     timestamps: true
})

module.exports = mongoose.model('user', userModel);
