const drinkMiddleware = (req,res,next) => {
    console.log(`Method request: ${req.method}`);
    next();
}

module.exports = { drinkMiddleware };