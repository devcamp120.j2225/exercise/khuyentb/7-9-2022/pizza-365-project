const userMiddleware = (req,res,next) => {
    console.log(`Method request: ${req.method}`);
    next();
}

module.exports = { userMiddleware };