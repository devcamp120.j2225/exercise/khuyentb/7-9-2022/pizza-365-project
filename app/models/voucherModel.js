// Import thư viện mongoose
const mongoose = require('mongoose');

// Khai báo class Schema
const Schema = mongoose.Schema;

// Khai báo voucher schema
const voucherModel = new Schema({
     // _id: {
     //      type: Schema.Types.ObjectId,
     //      unique: true
     // },
     maVoucher: {
          type: String,
          unique: true,
          required: true
     },
     phanTramGiamGia: {
          type: Number,
          required: true
     },
     ghiChu: {
          type: String,
          required: false
     },
}, {
     timestamps: true
})

module.exports = mongoose.model('vouchers', voucherModel);
