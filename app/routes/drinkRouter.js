// Import express 
const express = require("express");
const { execPath } = require("process");

// Import middleware
const {drinkMiddleware} = require("../middlewares/drinkMiddleware");

// Import controller 
const {
    getAllDrinks,
    getDrinkByID,
    createDrink,
    updateDrink,
    deleteDrink
} = require("../controllers/drinkControllers");
const { appendFile } = require("fs");

// create router
const drinkRouter = express.Router();

// su dung middleware
drinkRouter.use(drinkMiddleware);

// get All drink
drinkRouter.get("/drinks", getAllDrinks);

// get Drink by ID
drinkRouter.get("/drinks/:drinkID", getDrinkByID);

// create Drink
drinkRouter.post("/drinks", createDrink);

// update Drink 
drinkRouter.put("/drinks/:drinkID", updateDrink);

// delete Drink
drinkRouter.put("/drinks/:drinkID", deleteDrink);

module.exports = {drinkRouter}