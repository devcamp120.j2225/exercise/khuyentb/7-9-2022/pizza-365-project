// Import thư viện express
const express = require('express');
const { userMiddleware } = require('../middlewares/userMiddleware');

// Import controller
const { getAllUsers,
     getUserByID,
     createUser,
     updateUser,
     deleteUser,
     getAllUsersLimit,
     getAllUsersSkip,
     getAllUsersSortedByFullName,
     getAllUsersSkipLimit,
     getAllUsersSortedSkipLimit } = require('../controllers/userControllers');

// Tạo router
const userRouter = express.Router();

// Sử dụng middleware
userRouter.use(userMiddleware);

// Get all users
userRouter.get('/users', getAllUsers);

// Get an user by ID
userRouter.get('/users/:userID', getUserByID);

// Create a new user
userRouter.post('/users', createUser);

// Update an user
userRouter.put('/users/:userID', updateUser);

// Delete an user
userRouter.delete('/users/:userID', deleteUser);

// Skip - Limit - Sort
// Get all user limit
userRouter.get('/limit-users', getAllUsersLimit);

// Get all user skip
userRouter.get('/skip-users', getAllUsersSkip);

// Get all users sorted
userRouter.get('/sort-users', getAllUsersSortedByFullName);

// Get all users skip limit
userRouter.get('/skip-limit-users', getAllUsersSkipLimit);

// Get all users sorted skip limit
userRouter.get('/sort-skip-limit-users', getAllUsersSortedSkipLimit);

module.exports = { userRouter };
